<?php


class HomePageCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
        $I->wantToTest('front page of my site');
        $I->amOnPage('/');
        $I->see('coming soon...');
    }
}
