# Fly testing #
[![build status](https://gitlab.com/runexes/fly/badges/develop/build.svg)](https://gitlab.com/runexes/fly/commits/develop)

Feel free to create an issue or send me a pull request if you have any "how-to" question or bug or suggestion when using it.

## Usage ##

### Quick start ###

Coming soon...

## License ##

This package is licensed under MIT license. See LICENSE for details.